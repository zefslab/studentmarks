﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMarks.App_Start;

namespace StudentMarks.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Jurnal", "Student", new { subjectId = DataManager.GetSubjects().OrderBy(x => x.Name).FirstOrDefault().Id });
        }

    }
}