﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMarks.App_Start;
using StudentMarks.Models;

namespace StudentMarks.Controllers
{
    public class StudentController : Controller
    {
        public RedirectToRouteResult Index()
        {
            return RedirectToAction("Jurnal", new { subjectId = DataManager.GetSubjects().OrderBy(x => x.Name).FirstOrDefault().Id});
        }
        // GET: Student
        public ViewResult Jurnal(Guid subjectId)
        {
            //готовиться список студентов с отметками по указанному предмету
            var studentsMarksBySubject = DataManager.GetJurnal()
                .Where(x => x.SubjectId == subjectId) //фильтруем все записи по указанному предмету
                .Join(DataManager.GetStudents(), //присоединяемая коллекция студентов
                    x => x.StudentId, x => x.Id, //коллекции объединяются по двум ключам
                    (mark, student) => new StudentWithMark//формируем объекты анонимного типа
                    {
                        Id = student.Id,
                        Name = student.Name,
                        Mark = mark.Mark
                    });

            ViewBag.SubjectId = subjectId;
            ViewBag.Subjects = DataManager.GetSubjects();

            return View(studentsMarksBySubject);
        }

        
        public RedirectToRouteResult SetMark(Guid studentId, Guid subjectId, Marks mark)
        {
            var _mark = DataManager.GetJurnal().FirstOrDefault(x => x.StudentId == studentId & x.SubjectId == subjectId);
            _mark.Mark = mark;

            return RedirectToAction("Jurnal", new {subjectId = subjectId });
        }
    }
}
