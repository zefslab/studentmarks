﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace StudentMarks.Models
{
    /// <summary>
    /// Студент с отметкой по какому либо приедмету.
    /// Этот класс используется для передаче представлению в жирнал.
    /// </summary>
    public class StudentWithMark
    {
        public Guid Id { get; set; }

        [DisplayName("ФИО")]
        public string Name { get; set; }

        [DisplayName("Оценка")]
        public Marks Mark { get; set; }

    }
}