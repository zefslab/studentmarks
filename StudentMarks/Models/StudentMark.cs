﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMarks.Models
{
    public class StudentMark
    {
        public Guid StudentId { get; set; }
        public Guid SubjectId { get; set; }
        public Marks Mark { get; set; }

    }
}