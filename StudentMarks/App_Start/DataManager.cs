﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMarks.Models;

namespace StudentMarks.App_Start
{
    /// <summary>
    /// Класс продуцирующий данные
    /// </summary>
    public class DataManager
    {
        static private IEnumerable<Student> _students;
        static private IEnumerable<Subject> _subjects;
        private static IList<StudentMark> _journal;

        static public IEnumerable<Student> GetStudents()
        {
            if (_students == null)
            {
                var studentNames = new string[]
                {
                    "Герасимов Иван",
                    "Фирсова Лиза",
                    "Клабов Дизмат",
                    "Серегин Павел",
                    "Кочетов Михаил",
                    "Ломова Галина",
                    "Ширкин Кирилл",
                    "Селезнева Ольга",
                    "Долгих Василий",
                };

                var students = new List<Student>();
                foreach (var name in studentNames)
                {
                    students.Add(new Student() { Name = name, Id = Guid.NewGuid() });
                }

                _students = students;
            }

            return _students;
        }

        static public IEnumerable<Subject> GetSubjects()
        {
            if (_subjects == null)
            {
                var subjectNames = new string[]
                {
                    "Математика",
                    "Литература",
                    "Физика",
                    "Физкультура",
                    "Философия",
                    "Химия",
                    "Информатика",
                    "Иностранный язык",
                    "Спец. предмет",
                };

                var subjects = new List<Subject>();
                foreach (var name in subjectNames)
                {
                    subjects.Add(new Subject { Name = name, Id = Guid.NewGuid() });
                }

                _subjects = subjects;
            }

            return _subjects;
        }

        public static IList<StudentMark> GetJurnal()
        {
            if (_journal == null)
            {
                _journal = new List<StudentMark>();

                ///Первоначальное заполнение журнала, если он не заполнялся ранее
                foreach (var subject in GetSubjects())
                {
                    foreach (var student in GetStudents())
                    {
                        _journal.Add(new StudentMark
                        {
                            SubjectId = subject.Id,
                            StudentId = student.Id,
                            Mark = 0
                        });
                    }
                }
            }
            return _journal;
        }

        /// <summary>
        /// Простановка отметки студенту в журнал
        /// <param name=mark"">оценка, проставленные студенту в форме по какому либо предмету</param>
        /// </summary>
        public static void SetMark(StudentMark mark)
        {
            ///Находим запись в журнале по  идентификатору студента и идентификатору предмета
            var record = _journal.FirstOrDefault(x => x.StudentId == mark.StudentId
                                        && x.SubjectId == mark.SubjectId);
            ///Проставляем оценку
            record.Mark = mark.Mark;
        }

    }
}